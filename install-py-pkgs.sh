# !/bin/bash
export APP_HOME="$(cd "`dirname "$0"`"; pwd)"

. "${APP_HOME}"/libexec/log.sh

. "${APP_HOME}/conf/default.conf"
. ${BUILD_ENV_INFO_CONF}
. ${BUILD_REQUIREMENT_CONF}

. "${APP_HOME}/conf/runtime-env-info.sh"
. "${APP_HOME}/libexec/run-py-venv.sh"

pip install -r "${APP_HOME}"/src/requirements.txt
