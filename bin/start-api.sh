# !/bin/bash
export APP_HOME="$(cd "`dirname "$0"`"/..; pwd)"

. "${APP_HOME}"/libexec/log.sh

. "${APP_HOME}/conf/default.conf"
. "${APP_HOME}/conf/env.conf"
. "${APP_HOME}/conf/runtime-env-info.sh"
. "${APP_HOME}/libexec/run-py-venv.sh"

. "${APP_HOME}/src/.s2i/environment"

cd "${APP_HOME}"/src

# === user env ===
# predict api port (default: 5000)
export PREDICTIVE_UNIT_SERVICE_PORT=5000
# metrics api port (default: 6000)
export PREDICTIVE_UNIT_METRICS_SERVICE_PORT=6000
# log level (default: INFO)
export SELDON_LOG_LEVEL=INFO
# ================

seldon-core-microservice \
    ${MODEL_NAME} \
    ${API_TYPE} \
    --service-type ${SERVICE_TYPE} \
    --persistence $PERSISTENCE
