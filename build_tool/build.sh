# !/bin/bash
export APP_HOME="$(cd "`dirname "$0"`"/..; pwd)"
export APP_NAME="$(basename $APP_HOME)"

. "${APP_HOME}/conf/default.conf"

# include log manipulation script start
. "${APP_HOME}"/libexec/log.sh
# include log manipulation script end

. "${APP_HOME}"/libexec/str-parser.sh
. "${APP_HOME}"/build_tool/build-utils.sh

function usage() {
    echo "[Installation]
    Usage: $(basename $0) [OPTIONS] ENV (dev|ut|uat|prod)
     e.g. $(basename $0) -b dev
    OPTIONS:
       -h|--help                             Show this message
       -b|--build                            Build project
       -c|--clean                            Clean build result (venv)
       -r|--rebuild                          Rebuild Project, Clean and Build
       -p|--python                           Python version (Default:2) e.g. --python 2.7
       -d|--clean-deps                       Clean dependences from dist, build, egg_info folder
       -t|--test                             Install test dependences (docs/requirements-dev.txt)
    "
}

if [ $# -eq 0 ]; then
    echo "Missing parameters..."
    usage
    exit 1
fi

args=$(getopt -o hrbcdtp: --long build,clean,rebuild,clean-deps,test,python:,help \
    -n 'build.sh' -- "$@")

if [ $? != 0 ]; then
    echo "terminating..." >&2
    exit 1
fi
eval set -- "$args"

while true; do
    case "$1" in
    -b | --build)
        BUILD_OPT="true"
        shift
        ;;
    -c | --clean)
        CLEAN_OPT="true"
        shift
        ;;
    -r | --rebuild)
        BUILD_OPT="true"
        CLEAN_OPT="true"
        shift
        ;;
    -p | --python)
        INPUT_PYTHON_VERSION="$2"
        shift 2
        ;;
    -d | --clean-deps)
        CLEAN_DEPS_OPT="true"
        shift
        ;;
    -t | --test)
        TEST_OPT="true"
        shift
        ;;
    -h | --help)
        usage
        exit
        ;;
    --)
        shift
        break
        ;;
    *)
        echo "internal error!"
        exit 1
        ;;
    esac
done

for arg; do
    INPUT_ENV=$arg
done

# check for required args
if [[ -z ${INPUT_ENV} ]] && [[ -n ${BUILD_OPT} ]]; then
    echo "$(basename $0): missing ENV : ${ENV}"
    usage
    exit 1
fi

function init_env_variable() {
    _read_from_env_conf
    _validate_and_setting_env_variable
}

function _read_from_env_conf() {
    if [[ -f ${BUILD_ENV_INFO_CONF} ]]; then
        . ${BUILD_ENV_INFO_CONF}
        log_info "read env.conf from ${BUILD_ENV_INFO_CONF} \n ENV=$ENV , PYTHON_VERSION=${PYTHON_VERSION}"
    else
        log_warn "file not found: ${BUILD_ENV_INFO_CONF}"
    fi
}

function _validate_and_setting_env_variable() {
    _validate_and_setting_default_python
    _validate_and_setting_default_env
}

function _validate_and_setting_default_python() {

    if [[ -z ${INPUT_PYTHON_VERSION} ]]; then
        if [[ -n ${PYTHON_VERSION} ]]; then
            # from env.conf
            log_info "Find PYTHON_VERSION version \`$PYTHON_VERSION\` from ${BUILD_ENV_INFO_CONF}"
        else
            DEFAULT_PYTHON_VERSION=2
            PYTHON_VERSION=$DEFAULT_PYTHON_VERSION
            log_info "Assign default version \`$PYTHON_VERSION\` to PYTHON_VERSION"
        fi
    else
        if [[ -n ${PYTHON_VERSION} ]]; then
            _validate_env_conf_and_user_keyin "PYTHON_VERSION" "$INPUT_PYTHON_VERSION" "$PYTHON_VERSION"
        fi
        PYTHON_VERSION=$INPUT_PYTHON_VERSION
    fi
}

function _validate_and_setting_default_env() {

    if [[ -z ${INPUT_ENV} ]]; then
        if [[ -n ${ENV} ]]; then
            # from env.conf
            log_info "Find ENV version \`$ENV\` from ${BUILD_ENV_INFO_CONF}"
        else
            echo "$(basename $0): missing ENV : ${INPUT_ENV}"
            usage
            exit 1
        fi
    else
        if [[ -n ${ENV} ]]; then
            _validate_env_conf_and_user_keyin "ENV" "$INPUT_ENV" "$ENV"
        fi
        ENV=$INPUT_ENV
    fi
}

function _validate_env_conf_and_user_keyin() {
    match_value_name=$1
    user_keyin_value=$2
    exist_in_conf_value=$3

    if [[ $user_keyin_value != $exist_in_conf_value ]]; then
        log_error "$match_value_name error, Existing in env.conf: \`$exist_in_conf_value\` does not match expected input: \`$user_keyin_value\`"
        exit 1
    fi
}

function build_project() {
    log_info "Start to build project"
    init_env_variable

    . "${APP_HOME}/build_tool/build.conf"

    build_py_project_func "${PYTHON_VERSION}"

    # Make sure the installation is successful before writing the configuration file
    update_to_env_conf "${ENV}" "${PYTHON_VERSION}"
    . ${BUILD_ENV_INFO_CONF}
}

function clean_project() {
    clean_deps
    log_info "Start to clean project"
    clean_project_func
}

function clean_deps() {
    log_info "Start to remove dependencies"
    clean_py_deps_func
}

function build_test() {
    log_info "Start to build project by execute build test"
    build_project

    log_info "Start to execute install test"
    init_env_variable
    . ${BUILD_REQUIREMENT_CONF}
    . "${APP_HOME}/conf/runtime-env-info.sh"
    . "${APP_HOME}/libexec/run-py-venv.sh"
    install_test_func
}

# call function

if [[ -n $CLEAN_OPT ]]; then
    clean_project
fi

if [[ -n $BUILD_OPT ]]; then
    build_project
fi

if [[ -n $CLEAN_DEPS_OPT ]]; then
    clean_deps
fi

if [[ -n $TEST_OPT ]]; then
    build_test
fi
