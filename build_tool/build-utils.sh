# !/bin/bash

function build_basic_project_func() {
    # parameter from default.conf

    cd "${APP_HOME}"

    log_info "mkdir -p $LOG_DIR"
    mkdir -p $LOG_DIR

}

function build_py_project_func() {
    log_info "Start to build $APP_NAME "

    python_version=$1

    build_basic_project_func

    if [[ -z $python_version ]]; then
        default_version=2
        log_warn "python_version arg is empty, default: $default_version"
        python_version=$default_version
    fi

    cd "${APP_HOME}"

    expected_python_version=$(_get_python_version_from_prefix_func $python_version)
    log_info "validate expected python return: $expected_python_version"

    if [[ -d ${PY_VENV} ]]; then
        exists_python_version=$(_get_python_version_from_virtualenv_func)
        log_info "Skip create virtualenv, virtualenv already exists: $PY_VENV, python version is: $exists_python_version"
        if [[ $expected_python_version != $exists_python_version ]]; then
            log_error "Existing version: \`$exists_python_version\` does not match expected version: \`$expected_python_version\`"
            exit 1
        fi
    else
        if [[ $expected_python_version != 127 ]]; then
            _install_virtualenv_func $python_version
        else
            log_error "validate python error!"
        fi
    fi

    install_py_project_func

}

function _get_python_version_from_prefix_func() {
    python_version=$1
    full_version=$(python${python_version} -c 'import sys; version=sys.version_info[:3]; print("{0}.{1}.{2}".format(*version))')

    if [[ $? == 127 ]]; then
        echo 127
    else
        echo $full_version
    fi
}

function _get_python_version_from_virtualenv_func() {
    # enter virtualenv
    source ${PY_VENV}/bin/activate

    py_version=$(python -c 'import sys; version=sys.version_info[:3]; print("{0}.{1}.{2}".format(*version))')
    # exit virtualenv
    deactivate

    echo $py_version
}

function _copy_pip_config_func() {
    cd "${APP_HOME}"

    cp .pip/pip-${ENV}.conf ${PY_VENV}/pip.conf
}

function _install_virtualenv_func() {

    python_version=$1

    python_main_version=${python_version:0:1}

    log_info "create python${python_version} venv : $PY_VENV"
    virtualenv -p python${python_version} --no-setuptools --no-wheel --never-download ${PY_VENV}

    _copy_pip_config_func

    # enter virtualenv
    source ${PY_VENV}/bin/activate

    log_info "upgrade pip in venv"
    log_info "install setuptools wheel in venv"
    # to avoid warning log : DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020.
    if [[ $python_main_version == 2 ]]; then
        ${PY_VENV}/bin/pip install --upgrade pip==18.1 --disable-pip-version-check --no-cache-dir
        ${PY_VENV}/bin/pip install setuptools==40.6.3 --disable-pip-version-check --no-cache-dir
        ${PY_VENV}/bin/pip install wheel==0.32.3 --disable-pip-version-check --no-cache-dir
    else
        ${PY_VENV}/bin/pip install --upgrade pip --disable-pip-version-check --no-cache-dir
        ${PY_VENV}/bin/pip install setuptools wheel --disable-pip-version-check --no-cache-dir
    fi

    # exit virtualenv
    deactivate

}

function install_py_project_func() {
    # enter virtualenv
    source ${PY_VENV}/bin/activate
    cd ${APP_HOME}

    # install moudule in main project
    log_info "install ${APP_NAME} by setup.py"

    log_info "python setup.py install"
    python setup.py install

    # exit virtualenv
    deactivate
}

function clean_py_deps_func() {
    if [ -d "$PY_VENV" ]; then
        # enter virtualenv
        source ${PY_VENV}/bin/activate
        # clean .egg folder or file
        log_info "run setup.py clean -e"
        cd ${APP_HOME}
        python setup.py clean -e
        # exit virtualenv
        deactivate
    fi
}

function clean_project_func() {
    log_info "clean root project $APP_NAME"

    if [ -d "$PY_VENV" ]; then
        log_info "delete dir $PY_VENV"
        rm -rf "$PY_VENV"
    fi

    if [ -f "${BUILD_ENV_INFO_CONF}" ]; then
        log_info "delete file ${BUILD_ENV_INFO_CONF}"
        rm "${BUILD_ENV_INFO_CONF}"
    fi
}

function install_test_func() {
    # enter virtualenv
    source ${PY_VENV}/bin/activate
    cd ${APP_HOME}

    log_info "python setup.py extra -k dev"
    python setup.py extra -k dev

    # exit virtualenv
    deactivate
}

function update_to_env_conf() {
    expect_env=$1
    expect_python_version=$2

    log_info "write ${expect_env} to ENV arg in $BUILD_ENV_INFO_CONF"
    echo "# This script is automatically generated at build time, please do not edit it." >$BUILD_ENV_INFO_CONF

    echo ENV=\"${expect_env}\" >>$BUILD_ENV_INFO_CONF

    log_info "write ${expect_python_version} to PYTHON_VERSION arg in $BUILD_ENV_INFO_CONF"
    echo PYTHON_VERSION=\"${expect_python_version}\" >>$BUILD_ENV_INFO_CONF
}
