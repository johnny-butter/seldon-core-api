# !/bin/bash
export APP_HOME="$(cd "`dirname "$0"`"/..; pwd)"
export APP_NAME="$(basename $APP_HOME)"

. "${APP_HOME}/conf/default.conf"

# include log manipulation script start
. "${APP_HOME}"/libexec/log.sh
# include log manipulation script end

. "${APP_HOME}"/libexec/str-parser.sh

# to be consistent with the start-job.sh
if [[ -z $APP_TYPE ]] ; then
    APP_TYPE="job"
fi
export APP_TYPE

function usage() {
    echo "[Installation]
    Usage: $(basename $0) [OPTIONS]
     e.g. $(basename $0)
    OPTIONS:
       -h|--help                          Show this message
       --run                              The arguments to pass to pytest, e.g. --run=\"-s -v --cov -o log_cli=true\" or --run=\"tests/test_xxxx.py -o log_cli=true\".
    "
}

args=$(getopt -o h --long run::,help \
    -n 'run-test.sh' -- "$@")

if [ $? != 0 ]; then
    echo "terminating..." >&2
    usage
    exit 1
fi
eval set -- "$args"

while true; do
    case "$1" in
    --run)
        case "$2" in
        "")
            PYTEST_ARGS=''
            shift 2
            ;;
        *)
            PYTEST_ARGS=$2
            shift 2
            ;;
        esac
        ;;
    -h | --help)
        usage
        exit
        ;;
    --)
        shift
        break
        ;;
    *)
        echo "internal error!"
        exit 1
        ;;
    esac
done

function _read_from_env_conf() {
    if [[ -f ${BUILD_ENV_INFO_CONF} ]]; then
        . ${BUILD_ENV_INFO_CONF}
        log_info "read env.conf from ${BUILD_ENV_INFO_CONF} \n ENV=$ENV , PYTHON_VERSION=${PYTHON_VERSION}"
    else
        log_warn "file not found: ${BUILD_ENV_INFO_CONF}"
        exit 1
    fi
}

function test_project() {

    log_info "Start to execute test project"
    _read_from_env_conf
    . ${BUILD_REQUIREMENT_CONF}
    . "${APP_HOME}/conf/runtime-env-info.sh"
    . "${APP_HOME}/libexec/run-py-venv.sh"
    _test_py_project "$PYTEST_ARGS"
}

function _test_py_project() {
    pytest_args=$1

    # enter virtualenv
    source ${PY_VENV}/bin/activate
    cd ${APP_HOME}

    opt_test_args="--disable-check "
    printf "\E[0;33;40m ================= [pytest] Start ================== \n"
    log_info "python setup.py test --pytest-args="$pytest_args" $opt_test_args"
    python setup.py test --pytest-args="$pytest_args" $opt_test_args

    printf "\E[0;33;40m ================= [pytest] End ================== \n"

    printf "\E[0;33;40m ================= [flake8] Start ================== \n"
    flake8
    printf "\E[0;33;40m ================= [flake8] End ================== \n"

    printf "\E[0;33;40m ================= [bandit] Start ================== \n"
    bandit -r cathay
    printf "\E[0;33;40m ================= [bandit] End ================== \n"

    # exit virtualenv
    deactivate
}

test_project
