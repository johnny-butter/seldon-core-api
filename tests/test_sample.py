import pytest
import os
from src.Main import Main


class TestSampleSuites:
    @pytest.fixture()
    def output_config_path(self):
        return os.getcwd() + "/var/config/config.pkl"

    @pytest.fixture()
    def input_config_path(self):
        return os.getcwd() + "/conf/env.conf"

    def test_hello_world(self):
        assert 1 + 1 == 2

    def test_predict(self):
        params = ['a', 'b', 'c']

        assert Main().predict(params, []) == params
