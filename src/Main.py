# encoding:utf-8
# TODO import python packages
# if you import packages in py files which in src folder, you need write the packages and version in src/requirements.txt
# after writing, run script (install-py-pkgs.sh) to install python packages.


class Main(object):
    """
    Model template. You can load your model parameters in __init__ from a location accessible at runtime
    """

    def __init__(self):
        """
        Add any initialization parameters. These will be passed at runtime from the graph definition parameters
        defined in your seldondeployment kubernetes resource manifest.
        """
        print("Initializing")

    def predict(self, X, features_names):
        """
        Return a prediction.
        Parameters
        ----------
        X : array-like
        feature_names : array of feature names (optional)
        """
        print("Predict called - will run idenity function")
        return X
